// color variables
export const COLOR_GREY = '#e1e1e1';
export const COLOR_RED = '#cc3333';
export const COLOR_GREEN = '#009900';

// font sizes
export const FONT_SIZE_XL = '36px';
export const FONT_SIZE_LG = '24px';
export const FONT_SIZE_MD = '18px';

// font weights
export const FONT_WEIGHT_STRONG = '600';
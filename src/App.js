import React, { Component } from 'react';
import './styles/App.css';
import Products from './assets/data/products.json';
import ProductGrid from './components/product-grid/product-grid';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <ProductGrid products={Products} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;

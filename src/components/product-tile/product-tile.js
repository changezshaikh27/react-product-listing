import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import * as variables from '../../styles/shared/styled-variables';

//#region Product Tile Styles

const buttonTagHeight = '55px';

const ProductTileContainer = styled.div`
display: flex;
flex-direction: column;
min-height: 400px;
padding-bottom: 20px;
`;

const ProductImageWrapper = styled.div`
display: flex;
justify-content: center;
`;

const ProductImage = styled.img`
margin: 10px 0;`;

const ProductDetailWrapper = styled.div`
margin: 15px 10px;
`;

const ProductDetail = styled.div`
display: flex;
justify-content: space-between;
align-items: center;
`;

const ProductName = styled.span`
font-size: ${variables.FONT_SIZE_LG};
font-weight: ${variables.FONT_WEIGHT_STRONG};
`;

const ProductPrice = styled.span`
font-size: ${variables.FONT_SIZE_XL};
`;

const ProductOfferContainer = styled.div`
margin: 10px 0;
min-height: ${buttonTagHeight};
`;

const ButtonTag = styled.button`
min-height: ${buttonTagHeight};
color: #fff;
border: none;
outline: none !important;
padding: 0 25px;
margin-right: 10px;
font-size: ${variables.FONT_SIZE_MD};
`;

const GreenButtonTag = ButtonTag.extend`
background-color: ${variables.COLOR_GREEN};
`;

const RedButtonTag = ButtonTag.extend`
background-color: ${variables.COLOR_RED};
`;

//#endregion

export default class ProductTile extends React.Component {
  render() {
    const product = this.props.productDetail;
    return (
      <ProductTileContainer>
        <ProductImageWrapper>
          <ProductImage src={window.location.origin + `/images/${product.productImage}`} alt={product.productName} />
        </ProductImageWrapper>
        <ProductDetailWrapper>
          <ProductOfferContainer>
            {product.isExclusive && <GreenButtonTag>Exclusive</GreenButtonTag>}
            {product.isSale && <RedButtonTag>Sale</RedButtonTag>}
          </ProductOfferContainer>
          <ProductDetail>
            <ProductName>{product.productName}</ProductName>
            <ProductPrice>{product.price}</ProductPrice>
          </ProductDetail>
        </ProductDetailWrapper>
      </ProductTileContainer>
    );
  }
}

ProductTile.propTypes = {
  productDetail: PropTypes.object.isRequired
}


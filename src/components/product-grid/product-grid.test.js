import React from 'react';
import ReactDOM from 'react-dom';
import {shallow} from 'enzyme';
import ProductGrid from './product-grid';
import Products from '../../assets/data/products.json';

describe('Product Grid Tests', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ProductGrid products={Products} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  test('it should have the correct number of options in the size dropdown', () => {
    const productGrid = shallow(<ProductGrid products={Products} />);
    expect(productGrid.state().productSizes.length).toBe(4);
  });

  test('it should display all the products on initial load', () => {
    const productGrid = shallow(<ProductGrid products={Products} />);
    expect(productGrid.state().currentProducts.length).toBe(8);
  });

  test('it should display the correct number of options when a size is selected', () => {
    const productGrid = shallow(<ProductGrid products={Products} />);
    productGrid.find('select').simulate('change', {target : {value: 'XS'}});
    expect(productGrid.state().currentProducts.length).toBe(4);
  });

});

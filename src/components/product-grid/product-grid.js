import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import ProductTile from '../product-tile/product-tile';
import * as variables from '../../styles/shared/styled-variables';

//#region Product Grid Styles

const HeaderDiv = styled.div`
min-height: 55px;
width: 100%;
background-color: #def1f4;
display: flex;
justify-content: space-between;
align-items: center;
flex-wrap: wrap;
padding: 10px;`;

const ProductContainer = styled.div`
margin-top: 10px;`;

const ProductTileWrapper = styled.div`
border: solid 1px ${variables.COLOR_GREY};
padding: 0;`;

//#endregion

/**
 * The Product Grid component, contains the filter and products
 * 
 * @class ProductGrid
 * @extends {React.Component}
 */
class ProductGrid extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      productSizes: [],
      currentProducts: this.props.products
    };
    this.renderProductList = this.renderProductList.bind(this);
    this.populateProductSizes = this.populateProductSizes.bind(this);
    this.renderProductSizes = this.renderProductSizes.bind(this);
    this.filterProductsBySize = this.filterProductsBySize.bind(this);
  }

  componentWillMount() {
    // populate the product sizes on component receiving products
    this.populateProductSizes();
  }

  /**
   * Renders the Product List
   * 
   * @returns 
   * @memberof ProductGrid
   */
  renderProductList() {
    return this.state.currentProducts.map(product => {
      return (
        <ProductTileWrapper className="col-xl-3 col-lg-4 col-sm-6 col-xs-12" key={product.index}>
          <ProductTile productDetail={product} />
        </ProductTileWrapper>
      )
    });
  }

  /**
   * Populates the product size list
   * 
   * @memberof ProductGrid
   */
  populateProductSizes() {
    const uniqueSizes = new Set();
    this.props.products.forEach(product => {
      uniqueSizes.add(...product.size);
    });
    this.setState({productSizes: Array.from(uniqueSizes)});
  }

  /**
   * Renders the product size options
   * 
   * @returns 
   * @memberof ProductGrid
   */
  renderProductSizes() {
    return this.state.productSizes.map(size => {
      return <option value={size} key={size}>{size}</option>
    });
  }

  /**
   * Filter the products list based on the value selected in the dropdown
   * 
   * @param {any} event 
   * @returns 
   * @memberof ProductGrid
   */
  filterProductsBySize(event) {
    const size = event.target.value;
    if(size === 'ALL') {
      this.setState({currentProducts: this.props.products});
      return;
    }
    const filteredProducts = this.props.products.filter(product => product.size.includes(size));
    this.setState({currentProducts: filteredProducts});
  }

  render() {
    return (
      <section className="product-grid">
        <HeaderDiv>
          <h2>Women's tops</h2>
          <select onChange={this.filterProductsBySize}>
            <option disabled>Filter by size</option>
            <option value="ALL">All</option>
            {this.renderProductSizes()}
          </select>
        </HeaderDiv>
        <ProductContainer className="container">
          <div className="row">
            {this.renderProductList()}
          </div>
        </ProductContainer>
      </section>
    )
  }
}

ProductGrid.propTypes = {
  products: PropTypes.array.isRequired
}

export default ProductGrid;